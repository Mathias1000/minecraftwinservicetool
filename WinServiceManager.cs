﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Arcane.Engine.Tools.WinServices;

namespace Arcane.Engine.Tools
{
    internal static class WinServiceManager
    {
        private const string AdvAPI32Name = "advapi32.dll";
        private const int SERVICE_WIN32_OWN_PROCESS = 0x00000010;



        [DllImport(AdvAPI32Name, EntryPoint = "OpenSCManagerA")]
        private static extern IntPtr OpenServiceManager(string MachineName, string DatabaseName, ServiceManagerRights DesiredAccess);
        private static IntPtr OpenServiceManager(ServiceManagerRights Rights)
        {
            var scman = OpenServiceManager(null, null, Rights);
            if (scman == IntPtr.Zero)
            {
                throw new ApplicationException("Could not connect to service control manager.");
            }

            return scman;
        }

        [DllImport(AdvAPI32Name, EntryPoint = "OpenServiceA", CharSet = CharSet.Ansi)]
        private static extern IntPtr OpenService(IntPtr ServiceManager, string ServiceName, ServiceRights DesiredAccess);

        [DllImport(AdvAPI32Name)]
        private static extern int CloseServiceHandle(IntPtr Handle);

        [DllImport(AdvAPI32Name, EntryPoint = "CreateServiceA")]
        private static extern IntPtr CreateService(IntPtr hSCManager, string
        lpServiceName, string lpDisplayName, ServiceRights dwDesiredAccess, int
        dwServiceType, ServiceBootFlag dwStartType, ServiceError dwErrorControl,
        string lpBinaryPathName, string lpLoadOrderGroup, IntPtr lpdwTagId, string
        lpDependencies, string lp, string lpPassword);



      
        


        public static bool IsServiceInstalled(string ServiceName)
        {
            var scman = OpenServiceManager(ServiceManagerRights.Connect);
            try
            {
                var service = OpenService(scman, ServiceName, ServiceRights.QueryStatus);
                if (service == IntPtr.Zero)
                    return false;

                CloseServiceHandle(service);
                return true;
            }
            finally
            {
                CloseServiceHandle(scman);
            }
        }
        public static bool Install(string ServiceName, string DisplayName, string Filename)
        {
            var scman = OpenServiceManager(ServiceManagerRights.Connect | ServiceManagerRights.CreateService);
            var success = false;
            try
            {
                var service = OpenService(scman, ServiceName, ServiceRights.QueryStatus | ServiceRights.Start);
                if (service == IntPtr.Zero)
                {
                    service = CreateService(scman, ServiceName, DisplayName,
                    ServiceRights.QueryStatus | ServiceRights.Start, SERVICE_WIN32_OWN_PROCESS,
                    ServiceBootFlag.DemandStart, ServiceError.Normal, Filename, null, IntPtr.Zero,
                    null, null, null);
                }


                success = (service != IntPtr.Zero);

                if (success)
                {
                    CloseServiceHandle(service);
                }
            }
            catch (Exception)
            {
                success = false;
            }


            CloseServiceHandle(scman);

            return success;
        }
    }
}