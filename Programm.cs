﻿using Arcane.Engine.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WinService.WinStatus
{
  public class Programm
    {
      
        public static void Main()
        {
            string ServerPath = "C:\\Minecraft\\Server\\ServerWatcher.exe";
            string ServiceName = "Minecraft";


            WatchThread Watch = new WatchThread();


           
            Console.WriteLine("Searching WinService");


           Console.WriteLine("-----------------------------------------------------------------------");
            Console.WriteLine("Starting engine...{0}", Environment.NewLine);



            Console.WriteLine("Searching windows service...");
            if (!WinServiceManager.IsServiceInstalled(ServiceName))
            {
                Console.WriteLine("Installing service...");


                var success = false;
                var msg = "";

                try
                {
                    success = WinServiceManager.Install(ServiceName, ServiceName, ServerPath);

                    if (!success)
                        throw new InvalidOperationException("Unknown error occured.");


                    msg = "Service upload success";
                }
                catch (Exception ex)
                {
                    success = false;
                    msg = String.Format("Service upload failed.{0}Check exception log for more informations.", Environment.NewLine);

                    Console.WriteLine( "Error installing service:");
                }
                finally
                {
           
                   

                    Environment.Exit(0);
                }
            }
            else
            {
                Console.Write("Windows service found. Starting up...{0}", Environment.NewLine);

                ServiceBase.Run(new WatchThread());

            }
        }
    }
}
