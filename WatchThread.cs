﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using System.ServiceProcess;

internal class WatchThread : ServiceBase
{
    private Thread? Watch;

    private bool IsRunning = true;

    private Process NowProcc;

    protected override void OnStart(string[] args)
    {
        RunServer();
        Watch = new Thread(Loop);
        Watch.Start();
    }
    protected override void OnStop()
    {
        IsRunning = false;
        NowProcc.Close();
    }
  
    private void RunServer()
    {
        NowProcc = new Process();
        NowProcc.StartInfo.FileName = "Java";
        NowProcc.StartInfo.WorkingDirectory = "C:\\Minecraft\\Server\\";
        NowProcc.StartInfo.Arguments = @"-Xmx4024M -Xms4024M -jar -jar server.jar nogui";
        NowProcc.StartInfo.UseShellExecute = false;
        NowProcc.Start();
   

    }
    private void Loop()
    {
        while (IsRunning)
        {
            NowProcc?.WaitForExit();

            RunServer();
        }
    }
}
